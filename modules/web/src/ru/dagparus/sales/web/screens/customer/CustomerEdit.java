package ru.dagparus.sales.web.screens.customer;

import com.haulmont.cuba.gui.screen.*;
import ru.dagparus.sales.entity.Customer;

@UiController("sales_Customer.edit")
@UiDescriptor("customer-edit.xml")
@EditedEntityContainer("customerDc")
@LoadDataBeforeShow
public class CustomerEdit extends StandardEditor<Customer> {
}